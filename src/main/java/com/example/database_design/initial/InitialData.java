package com.example.database_design.initial;

import com.example.database_design.entity.*;
import com.example.database_design.repository.*;
import org.springframework.boot.CommandLineRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class InitialData implements CommandLineRunner {

    private final DepartmentRepository departmentRepository;
    private final BatchRepository batchRepository;
    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final SemesterRepository semesterRepository;
    private final EnrollmentRepo enrollmentRepo;

    public InitialData(DepartmentRepository departmentRepository, BatchRepository batchRepository, CourseRepository courseRepository, StudentRepository studentRepository, SemesterRepository semesterRepository, EnrollmentRepo enrollmentRepo) {
        this.departmentRepository = departmentRepository;
        this.batchRepository = batchRepository;
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
        this.semesterRepository = semesterRepository;
        this.enrollmentRepo = enrollmentRepo;
    }

    @Override
    public void run(String... args) throws Exception {
//        Batch b = new Batch("40");
//        batchRepository.save(b);
//
//        Department d = new Department("CSE");
//        departmentRepository.save(d);
//
//        Course c1 = new Course("C++");
//        Course c2 = new Course("C");
//        Course c3 = new Course("Java");
//        courseRepository.save(c1); courseRepository.save(c2); courseRepository.save(c3);
//
//        Student student = new Student("Faisal");
//        student.setBatch(b);
//        student.setDepartment(d);
//        studentRepository.save(student);
//
//        Semester semester = new Semester("Sp20-1st");
//        semesterRepository.save(semester);
//
//        List<Course> coursesTemp = new ArrayList<>();
//        coursesTemp.add(c1);coursesTemp.add(c2);coursesTemp.add(c3);
//
//        Enrollment enrollment = new Enrollment();
//        enrollment.setCourses(coursesTemp);
//        enrollment.setStudent(student);
//        enrollment.setSemester(semester);
//        enrollmentRepo.save(enrollment);

        Stream.of("CSE", "EEE", "ETE", "MATH", "ELL", "BANGLA")
                .filter(s -> !departmentRepository.existsByDepartmentName(s))
                .map(Department::new)
                .map(departmentRepository::save)
                .forEach(l -> {
                    System.out.print(l.getDepartmentName()+" ");
                });

        Stream.of("32", "34", "36", "38", "39", "40")
                .filter(s -> !batchRepository.existsByBatchName(s))
                .map(Batch::new)
                .map(batchRepository::save)
                .forEach(l -> {
                    System.out.print(" ");
                });

        Stream.of("CSE-1001", "CSE-1002", "CSE-1003","CSE-1005", "CSE-1006", "CSE-1007", "EEE-1001", "EEE-1002", "EEE-1003","ETE-1001", "ETE-1002", "ETE-1003")
                .filter(s -> !courseRepository.existsByCourseName(s))
                .map(Course::new)
                .map(courseRepository::save)
                .forEach(l -> {
                    System.out.print(" ");
                });
//        studentRepository.deleteAll();
//        Student student = new Student("Faisal");
//        studentRepository.save(student);
    }
}
