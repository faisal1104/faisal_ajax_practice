package com.example.database_design.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter@Setter@NoArgsConstructor
public class Semester implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long semesterId;

    @Column
    private String semesterNumber;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "enrollment_semester",
            joinColumns = @JoinColumn(name = "semester_id"),
            inverseJoinColumns = @JoinColumn(name = "enroll_id"))
    private List<Enrollment> enrollments;

    public Semester(String semesterNumber) {
        this.semesterNumber = semesterNumber;
    }
}
