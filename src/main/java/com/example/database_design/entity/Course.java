package com.example.database_design.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data@NoArgsConstructor
public class Course implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long courseId;
    @Column
    private String courseName;

    public Course(String courseName) {
        this.courseName = courseName;
    }
}
