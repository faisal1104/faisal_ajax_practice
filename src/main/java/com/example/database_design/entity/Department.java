package com.example.database_design.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data@NoArgsConstructor
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long depId;
    @Column
    private String departmentName;

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }
}
