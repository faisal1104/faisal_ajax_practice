package com.example.database_design.controller;

import com.example.database_design.dto.StudentDto;
import com.example.database_design.entity.Batch;
import com.example.database_design.entity.Department;
import com.example.database_design.entity.Student;
import com.example.database_design.repository.BatchRepository;
import com.example.database_design.repository.DepartmentRepository;
import com.example.database_design.repository.StudentRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/s")
public class AjaxController {

    private final StudentRepository studentRepository;
    private final DepartmentRepository departmentRepository;
    private final BatchRepository batchRepository;

    public AjaxController(StudentRepository studentRepository, DepartmentRepository departmentRepository, BatchRepository batchRepository) {
        this.studentRepository = studentRepository;
        this.departmentRepository = departmentRepository;
        this.batchRepository = batchRepository;
    }

    @GetMapping("/")
    public String getAll(Model m) {
        m.addAttribute("departmentList", departmentRepository.findAll());
        m.addAttribute("batchList", batchRepository.findAll());
        return "student/manage-student";
    }

    @GetMapping(value = "/getAll", consumes = "application/json", produces = "application/json")
    public @ResponseBody List<StudentDto> getAllStudent_onLoad() {
        List<Student> studentList = studentRepository.findAll();
        List<StudentDto> studentDtoList = new ArrayList<>();

        for (Student s: studentList) {
            StudentDto sTemp = new StudentDto();
            BeanUtils.copyProperties(s, sTemp);
            sTemp.setDepName(s.getDepartment().getDepartmentName());
            sTemp.setBatchName(s.getBatch().getBatchName());
            studentDtoList.add(sTemp);
        }
        return studentDtoList;
    }


    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody StudentDto saveStudent(@RequestBody StudentDto studentDto) {
        Department department = departmentRepository.getOne(studentDto.getDepId());
        Batch batch = batchRepository.getOne(studentDto.getBatchId());

        Student student = new Student();
        BeanUtils.copyProperties(studentDto, student);
        student.setDepartment(department);
        student.setBatch(batch);
        studentRepository.save(student);
        studentDto.setDepName(department.getDepartmentName());
        StudentDto studentDto1 = new StudentDto();
        BeanUtils.copyProperties(student, studentDto1);
        studentDto1.setDepName(department.getDepartmentName());
        studentDto1.setBatchName(batch.getBatchName());
        return studentDto1;
    }
}
