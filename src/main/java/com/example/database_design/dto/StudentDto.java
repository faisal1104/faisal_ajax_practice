package com.example.database_design.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor@Getter@Setter
public class StudentDto {
    private long stuId;
    private String studentName;
    private long depId;
    private String depName;
    private long batchId;
    private String batchName;
}
