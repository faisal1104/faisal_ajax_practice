package com.example.database_design;

import com.example.database_design.initial.InitialData;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseDesignApplication {

    public static void main(String[] args) {
        SpringApplication.run(new Class[]{DatabaseDesignApplication.class, InitialData.class}, args);
    }
}
